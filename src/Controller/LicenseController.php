<?php

namespace App\Controller;

use App\Service\LicenseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
final class LicenseController extends AbstractController
{
    #[Route('', name: 'home')]
    public function index(LicenseService $licenseService): Response
    {
        return $this->render('license/index.html.twig', [
            'about' => $licenseService->about(),
            'versions' => $licenseService->listVersions(),
            'latest' => $licenseService->load($licenseService->latest()),
            'usages' => $licenseService->listUsages(),
        ]);
    }

    #[Route('/license.{_format}', name: 'license_latest')]
    #[Route('/license/v{version}.{_format}', name: 'license_version')]
    #[Route('/license/{version}.{_format}', name: 'license_version_no_v')]
    public function license(Request $request, LicenseService $licenseService, string $version = null): Response
    {
        $license = $licenseService->load($version, $request->get('c'));

        if ($request->getRequestFormat() === 'md') {
            return new Response(
                $license['raw'],
                Response::HTTP_OK,
                ['content-type' => 'text/markdown'],
            );
        }

        return $this->render(
            $request->getRequestFormat() === 'tldr'
                ? 'license/summary.html.twig'
                : 'license/license.html.twig',
            [
                'license' => $license,
                'about' => $licenseService->about(),
            ]
        );
    }
}
